- Controllers for the Forklift Example
Delayed Version
 Main controller is obtained composing:
 - DelayedController1 
 - DelayedController2
 - DelayedController3
 Simplified versions are shown in:
 - Controller Delayed Composed Simplified 1
 - Controller Delayed Composed Simplified 2
 - Controller Delayed Composed Simplified 3
 These controllers exhibit partial composed automata obtained from: 
 - DelayedController1 
 - DelayedController2
 - DelayedController3 

 Continuous Version
 - ControllerContinuous1
 - ControllerContinuousComposedSimplified1 (composing with the delayed Controller)

Controllers for the DC Motor Example
 - ControllerDCMotor
 - ControllerDCMotorSharedResources
 - ControladorDcMotorComposedSimplified (composing controllers)

All the controllers must be executed with the GOAL tool.