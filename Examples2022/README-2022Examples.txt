The .gff corresponds to the GOAl tool.
The .lts files corresponds to the parallel MTSA tool.

The controller for the DEKKEr algorithm is obtained combining the following files:
- DekkerController.gff 
- DekkerController3.gff

The controller for the load balancer is obtained combining the following files:
- LoadBalancerClient.gff
- LoadBalancerServer.gff

