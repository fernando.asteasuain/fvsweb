Package.describe({
  name: 'joint-4fvs',
  version: '1.0.0',
  summary: '',
  git: '',
  documentation: 'README.md'
});

Package.onUse(function(api) {
  api.versionsFrom('1.3.4.1');
  // api.use('ecmascript');
  // api.mainModule('meteor-joint-4fvs.js');
  // api.use('jquery@1.11.4', 'client');
  // api.use('stevezhu:lodash', 'client');
  // api.use('backbone', 'client');

  api.use('lodash-4fvs', 'client');
  api.use('backbone-4fvs', 'client');

  api.addFiles(['joint.js',
                'joint.shapes.fsa.js',
                'joint.fvs.js',
                'joint.css'], 'client');
});

Package.onTest(function(api) {
  // api.use('ecmascript');
  // api.use('tinytest');
  // api.use('meteor-joint-4fvs');
  // api.mainModule('meteor-joint-4fvs-tests.js');
});
