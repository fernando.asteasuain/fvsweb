#!/bin/bash
trap ' ' INT

echo "=> Cleaning up packages ..."
rm ./packages/lodash-4fvs
rm ./packages/backbone-4fvs
rm ./packages/joint-4fvs
rm ./packages/colorpicker-4fvs
rm ./packages/jruler-4fvs
echo "=> It's DONE!"

echo "=> Installing packages ..."
ln -s ../meteor-packages/lodash-4fvs packages/lodash-4fvs
ln -s ../meteor-packages/backbone-4fvs packages/backbone-4fvs
ln -s ../meteor-packages/joint-4fvs packages/joint-4fvs
ln -s ../meteor-packages/colorpicker-4fvs packages/colorpicker-4fvs
ln -s ../meteor-packages/jruler-4fvs packages/jruler-4fvs

echo "=> Starting METEOR!!"
meteor


