
// svg2png = function(svg /*:String*/) {

//     var canvas = document.createElement("canvas");
//     var ctx = canvas.getContext("2d");

//     var img = document.createElement("img");
//     img.onload = function() {
//         ctx.drawImage(img, 0, 0);

//         var dt = canvas.toDataURL('image/png');
//         /* Change MIME type to trick the browser to downlaod the file instead of displaying it */
//         dt = dt.replace(/^data:image\/[^;]*/, 'data:application/octet-stream');

//         /* In addition to <a>'s "download" attribute, you can define HTTP-style headers */
//         dt = dt.replace(/^data:application\/octet-stream/, 'data:application/octet-stream;headers=Content-Disposition%3A%20attachment%3B%20filename=Canvas.png');

//         console.log(dt);
//         window.location = dt;
//     };

//     img.setAttribute("src", "data:image/svg+xml;base64," + btoa(svg));
// };

svg2png = function(svg /*:String*/) {

    svg = prepareSVGforExport(svg);

    var canvas = document.createElement("canvas");
    canvg(canvas, svg);

    var pngURL = canvas.toDataURL('image/png')
        .replace(/^data:image\/[^;]*/, 'data:application/octet-stream')
        .replace(/^data:application\/octet-stream/, 'data:application/octet-stream;headers=Content-Disposition%3A%20attachment%3B%20filename=Canvas.png');

    return pngURL;
};

var prepareSVGforExport = function(svg) {
    $svg = $(svg);

    // set white background color
    // http://stackoverflow.com/questions/11293026/default-background-color-of-svg-root-element
    $svg.prepend('<rect width="100%" height="100%" style="fill:rgb(255,255,255);"/>');

    // remove animation-purpose circle
    $svg.find('.outer-circle').remove();
    // remove shadow attribute
    $svg.find('.inner-circle').removeAttr('filter');
    // remove Joint.js links editing tools
    $svg.find('.link-tools').remove();
    $svg.find('.marker-vertices').remove();
    $svg.find('.marker-arrowheads').remove();

    //http://stackoverflow.com/questions/652763/how-do-you-convert-a-jquery-object-into-a-string
    svg = $svg.prop('outerHTML');

    return svg;
}