
Template.navbarDocument.onRendered(function() {

    console.log("Navbar Document initialized successfully!!");

    // span to input and input to span
    new SpanInput('.diagram-name');

});

Template.canvas.onRendered(function() {
    $('.diagram-name').text(graph._gtxfvs.title);
});

/******************************************************************************
 * http://stackoverflow.com/questions/16374057/converting-span-to-input
 * http://jsfiddle.net/2D9FW/
 */
var SpanInput = function(selector) {

    var switchToInput = function () {
        var $input = $("<input>", {
            val: $(this).text(),
            type: 'text',
            placeholder: 'Diagrama sin nombre...'
        });
        $input.addClass("diagram-name");
        $input.addClass("form-control");
        $(this).replaceWith($input);

        // events
        $input.on('keypress', function(evt) {
            var EnterKey = '13';
            if (evt.keyCode == EnterKey) {
                $input.blur();
            }
        });
        $input.on('change', function(evt) {
            graph._gtxfvs.title = $(this).val();
            console.log("Title changed");
        })
        $input.on("blur", switchToSpan);

        //
        $input.select();
    };

    var highlightSpan = function(e) {
        var $target = $(this);
        $target.css({
            'border': '1px solid #ccc'
        });
    };

    var unhighlightSpan = function(e) {
        var $target = $(this);
        $target.css({
            'border': '1px solid #fff'
        });
    }

    var switchToSpan = function () {
        var $span = $("<span>", {
            text: $(this).val()
        });
        $span.addClass("diagram-name");
        $span.addClass("form-control");
        $(this).replaceWith($span);
        $span.on("click", switchToInput);
        $span.mousemove(highlightSpan);
        $span.mouseout(unhighlightSpan);
    };

    var jElem = $(selector);
    jElem.on("click", switchToInput);
    jElem.mousemove(highlightSpan);
    jElem.mouseout(unhighlightSpan);
};