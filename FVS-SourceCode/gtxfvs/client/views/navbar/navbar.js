
Template.navbar.onRendered(function() {

    $('#downloadAs').on('click', function(evt) {
        console.log('Downloading!!');

        // view in xml
        var serializer = new XMLSerializer();
        var svgString = serializer.serializeToString(paper.svg);

        // from xml to png (URL)
        var pngURL = svg2png(svgString);

        // let's create an anchor to 'click'
        var anchor = $(document.createElement("a"));
        anchor.attr('href', pngURL);
        anchor.attr('download', graph._gtxfvs.title + '.png');
        // fire event!!
        anchor[0].click();
    });

    $('#toJSON').on('click', function(evt) {
        console.log('Downloading JSON!!');

        var jsonRepresentation = graph.toJSON();
        console.log(jsonRepresentation);
    });
});
