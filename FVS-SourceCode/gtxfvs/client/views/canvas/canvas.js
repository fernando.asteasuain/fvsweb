
graph = null;
paper = null;

// http://stackoverflow.com/questions/1818467/how-to-apply-100-height-to-div
var resizeCanvas = function(className){
    var bottomMargin = 50;

    $(className).height($(window).height() - $(className).offset().top - bottomMargin);
    $(window).resize();
};

var InfinitePaper = function($canvasContainer, paper) {
    $canvasContainer.append("<div id='moreWidth'></div>");
    $canvasContainer.append("<div id='moreHeight'></div>");

    var resizePaperIfNeeded = function() {

        //var $canvasContainer = $('.canvas-container');
        var $moreHeight = $("#moreHeight");
        var $moreWidth = $("#moreWidth");

        // if moreHeight is visible then we add 1/3 of the paper height
        var mhPos = $moreHeight.position();
        if(mhPos.top < $canvasContainer.height()) {
            console.log(paper.options.height);

            console.log("More paper (height), please!!");
            var currentHeight = paper.options.height;
            paper.setDimensions(paper.options.width, currentHeight + currentHeight / 3);

            console.log(paper.options.height);
        }

        var mwPos = $moreWidth.position();
        if(mwPos.left < $canvasContainer.width()) {
            console.log(paper.options.width);

            console.log("More paper (width), please!!");
            var currentWidth = paper.options.width;
            paper.setDimensions(currentWidth + currentWidth / 3, paper.options.height);

            console.log(paper.options.width);
        }
    }

    //
    $canvasContainer.smartscroll(function(evt) {
        resizePaperIfNeeded();
    });
}

Template.canvas.onRendered(function() {

    // listen for smartresize events
    $(window).smartresize(function() {
        resizeCanvas('.canvas-container')
    });
    // initial resize
    resizeCanvas('.canvas-container');

    //
    var $ruler = $('.canvas-container')
                    .ruler({
                        showCrosshair: false,
                        showMousePos: false
                    });


    // Model (global object)
    graph = new joint.dia.Graph;
    graph._gtxfvs = {
        title: 'Untitled diagram'
    }

    // get container size
    var containerSize = {
        height: $('.canvas-container').height(),
        width: $('.canvas-container').width()
    };
    // calculate the paper size
    var paperSize = {
        height: containerSize.height + containerSize.height / 3,
        width: containerSize.width + containerSize.width / 3
    };
    // View (global object)
    paper = new joint.dia.FVSPaper({
        el: $('#paper'),
        gridSize: 1,
        model: graph,
        width: paperSize.width,
        height: paperSize.height,
        //'viewport-fill': '#dedede',

        // http://jointjs.com/api#joint.dia.LinkView:addVertex
        linkView: joint.dia.LinkView.extend({
            pointerdblclick: function(evt, x, y) {
                if (V(evt.target).hasClass('connection') || V(evt.target).hasClass('connection-wrap')) {
                    this.addVertex({ x: x, y: y });
                }
            }
        }),
        interactive: function(cellView) {
            if (cellView.model instanceof joint.dia.Link) {
                // Disable the default vertex add functionality on pointerdown.
                return { vertexAdd: false };
            }
            return true;
        }
    });

    // handle zoom in/out
    paper.on('scale', function(factorX, factorY) {
        console.log("paper scale event:");
        console.log(factorX);
        console.log(factorY);

        $ruler.scale(factorX, factorY);
    });

    new InfinitePaper($('.canvas-container'), paper);
});
