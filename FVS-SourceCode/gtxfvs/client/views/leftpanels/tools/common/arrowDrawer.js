
PrecedenceArrowDrawer = function(paper, propertiesEditor) {
    return new ArrowDrawer(paper, joint.shapes.fvs.Arrow, propertiesEditor, {
        init: "DRAWER::Precedence-Arrow Initiallized",
        stop: "DRAWER::Precedence-Arrow Stopped"
    });
}

ImmediateNextArrowDrawer = function(paper, propertiesEditor) {
    return new ArrowDrawer(paper, joint.shapes.fvs.ImmediateNextArrow, propertiesEditor, {
        init: "DRAWER::ImmediateNext-Arrow Initiallized",
        stop: "DRAWER::ImmediateNext-Arrow Stopped"
    });
}

ImmediatePreviousArrowDrawer = function(paper, propertiesEditor) {
    return new ArrowDrawer(paper, joint.shapes.fvs.ImmediatePreviousArrow, propertiesEditor, {
        init: "DRAWER::ImmediatePrevious-Arrow Initiallized",
        stop: "DRAWER::ImmediatePrevious-Arrow Stopped"
    });
}

ArrowDrawer = function(paper, ArrowType, propertiesEditor, logTexts) {

    var paper = paper;
    var tooltip = null;

    var sourceState = null;
    var targetState = null;

    var onMouseOver = function(cellView, evt, x, y) {
        // http://stackoverflow.com/questions/25230987/to-change-mouse-cursor-on-click-in-jointjs
        cellView.$el.addClass('selectingState');

        // var elem = graph.getCell(cellView.model.id);
        // elem.attr({
        //     circle: {
        //         'fill': '#FFFF1E',
        //         'stroke': '#FFBB1E',
        //         'stroke-width': 3
        //     },
        // });
    };

    var onMouseOut = function(cellView, evt) {

        cellView.$el.removeClass('selectingState');

        //
        // var elem = graph.getCell(cellView.model.id);
        // elem.attr({
        //     circle: {
        //         'fill': '#000',
        //         'stroke-width': 0
        //     }
        // });
    };

    var onPointerUp = function(cellViewUp, evt) {

        if (sourceState == null) {
            sourceState = cellViewUp.model.id;

            tooltip.text("Seleccionar DESTINO");

        } else if (targetState == null) {
            targetState = cellViewUp.model.id;

            // http://jointjs.com/demos/links
            //{ position: 0.5, attrs: { text: { text: 'bli!!', fill: 'white', 'font-family': 'sans-serif' }, rect: { stroke: '#31d0c6', 'stroke-width': 20, rx: 5, ry: 5 } } }]
            var arrow = new ArrowType({
                source: { id: sourceState },
                target: { id: targetState },
                labels: [{ position: 0.5 , attrs: { text: { text: 'NOT()' || '', 'font-weight': 'bold' } } }]
            });
            arrow._gtxfvs.propertiesEditor = propertiesEditor;
            graph.addCell(arrow);

            sourceState = null;
            targetState = null;

            tooltip.text("Seleccionar ORIGEN");

        } else {
            console.log("Hey!! What am I doing here?!");
        }
    }

    return {
        init: init,
        stop: stop
    }

    function init() {
        console.log(logTexts.init);

        //
        tooltip = new HintLabel('#paper');
        tooltip.text("Seleccionar ORIGEN");

        // Set a crosshair cursor
        paper.$el.addClass('selectingState');

        //
        paper.on('cell:mouseover', onMouseOver);
        paper.on('cell:mouseout', onMouseOut);
        paper.on('cell:pointerup', onPointerUp);
    }

    function stop() {
        console.log(logTexts.stop);

        paper.$el.removeClass('selectingState');

        paper.off('cell:mouseover', onMouseOver);
        paper.off('cell:mouseout', onMouseOut);
        paper.off('cell:pointerup', onPointerUp);

        tooltip.destroy();
    }

    /**************************************************************************
     * No API here, please.
     */

};