Template.toolStateProperties.onRendered(function() {

    var item_id = Session.get('idSelected');
    var item = graph.getCell(item_id);

    // label
    var input = $('#selectedStateLabel');
    input.on('keypress', function(evt) {
        var EnterKey = '13';

        if (evt.keyCode == EnterKey) {
            input.blur();
        }
    });
    input.on("change", function(evt) {
        console.log(evt);
        // invoking label with call to set the context
        item._gtxfvs.label.call(item, input.val());
    });

    // color
    // var domColorPicker = $('#colorpicker');
    // domColorPicker.colorpicker();
    // domColorPicker.on('changeColor', function(evt) {
    //     var color = evt.color;
    //     console.log(color);
    //     item._gtxfvs.color.call(item, color.toHex());
    // });

    new ColorPicker({
        domContainer: this.$(".colorpicker-component").get(0),
        color: item._gtxfvs.color.call(item),
        onChangeColor: function(evt) {
            var color = evt.color;
            console.log(color);
            item._gtxfvs.color.call(item, color.toHex());
        }
    });

});

