Template.toolArrowProperties.onRendered(function() {

    var item_id = Session.get('idSelected');
    var item = graph.getCell(item_id);

    // label
    var input = this.$('#selectedStateLabel');
    input.on('keypress', function(evt) {
        var EnterKey = '13';

        if (evt.keyCode == EnterKey) {
            input.blur();
        }
    });
    input.on("change", function(evt) {
        console.log(evt);
        // invoking label with call to set the context
        item._gtxfvs.label.call(item, input.val());
    });

    // colorpicker
    new ColorPicker({
        domContainer: this.$(".colorpicker-component").get(0),
        color: item._gtxfvs.color.call(item),
        onChangeColor: function(evt) {
            var color = evt.color;
            console.log(color);
            item._gtxfvs.color.call(item, color.toHex());
        }
    });

    // smooth
    this.$(':checkbox').change(function() {
        console.log("hey hey check!!" + this.checked);
        // link.set('smooth', true)
        item._gtxfvs.smooth.call(item, this.checked);
    });

    // width
    var widthInput = this.$('#widthProp');
    widthInput.on('change', function(evt) {
        item._gtxfvs.width.call(item, widthInput.val());
    });
});