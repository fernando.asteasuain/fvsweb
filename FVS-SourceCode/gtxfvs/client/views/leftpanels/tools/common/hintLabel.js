HintLabel = function(domNode) {

    var paperDomNode = $(domNode);
    var hint =  $('<div class="label label-info label-tooltip"></div>')
                    .appendTo(paperDomNode);
    text("No text for you!! :)");

    // listen to the mouse!!
    paperDomNode.mousemove(function(e) {
        hint.css("top", e.offsetY - 15);
        hint.css("left", e.offsetX - hint.width() / 2);
    });

    var api = {
        text: text,
        destroy: destroy
    }

    return api;

    function text(text) {
        hint.empty();
        hint.prepend($('<span>' + text + '</span>'));
    }

    function destroy() {
        hint.remove();
    }
}