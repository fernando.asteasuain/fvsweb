
StateDrawer = function(paper, propertiesEditor) {

    var dummyState = null;

    var onPointerUp = function(cellView, evt) {
        // Dear dummyState: You're no longer dummy!!
        promoteDummyState();
        // Now we have a new dummy in town!!
        createDummyState(evt.offsetX, evt.offsetY);
    };

    var onPointerMove = function(evt, x, y) {

        if(dummyState == null) {
            createDummyState();
        }
        centerDummyStateInMousePosition(x,y);
    };

    return {
        init: init,
        stop: stop
    }


    function init() {
        console.log("DRAWER::State Initiallized");

        paper.on('cell:pointerup', onPointerUp);
        paper.on('blank:pointermove', onPointerMove);
    }

    function stop() {
        console.log("DRAWER::State Stopped");

        paper.off('cell:pointerup', onPointerUp);
        paper.off('blank:pointermove', onPointerMove);

        // still dummy? ... bye bye!!
        removeDummyState();
    }

    /**************************************************************************
     * No API here, please.
     */
    function createDummyState(x, y) {
        var options = {};
        // if(x != null && y != null) {
        //     options = {
        //         position: { x: x, y: y }
        //     };
        // }
        options = {
            attrs: {
                text : { text: "State A", fill: '#000000', 'font-weight': 'normal' }
            }
        };
        dummyState = new joint.shapes.fvs.StateLabel(options);
        dummyState._gtxfvs.propertiesEditor = propertiesEditor;
        //dummyState._gtxfvs.prototype = dummyState;

        if(x != null && y != null) {
            centerDummyStateInMousePosition(x, y);
        }
        graph.addCell(dummyState);
    }

    function promoteDummyState() {
        dummyState = null;
    }

    function removeDummyState() {
        if(dummyState != null) {
            dummyState.remove();
            dummyState = null;
        }
    }

    function centerDummyStateInMousePosition(x, y) {
        var bbox = dummyState.getBBox();
        var correction = {
            x: bbox.width / 2,
            y: bbox.height / 2
        }
        dummyState.position(x - correction.x,
                            y - correction.y);
    }
};