
Template.tools.onRendered(function() {
    console.log("Tools template rendered!!");

    // Init FVS joint-plugin in /client/lib/joint.shapes.fvs.js
    // initFVS();

    $('#tools-container > .btn').each(function(index) {

        var button = $(this);
        button.on("click", function(e) {

            // http://stackoverflow.com/questions/1950038/jquery-fire-event-if-css-class-changed
            button.addClass('active')
                .siblings().removeClass('active').trigger('deactivated');
        });
    });
});
