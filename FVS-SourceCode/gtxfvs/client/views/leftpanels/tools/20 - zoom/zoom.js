
var scales = [0.1, 0.3, 0.6, 1, 1.5, 2, 3, 4, 5];
var scaleIxMin = 0;
var scaleIx11 = 3; //1:1 No zoom
var scaleIxMax = scales.length - 1;
var scaleIx = scaleIx11;

Template.tools.onRendered(function() {

    // we create the toolbar button
    //var container = $('#tools-container');
    var container = $('.canvas-container');
    Blaze.render(Template.toolZoom, container.get(0));
    console.log("Tool Zoom initialized successfully!!");
});

Template.toolZoom.onRendered(function() {

    var canvas = $('.canvas-container');
    var toolContainer = $('#toolZoom-container');

    var rulerMargin = 18; //INPOT
    var toolMargin = 30;

    var top = 0;
    var left = canvas.width() - toolContainer.width() - toolMargin; //right=0

    var placeToolOnTopRightCorner = function() {
        toolContainer.css({
            'top': top + rulerMargin + canvas.scrollTop(),
            'left': left + (rulerMargin + canvas.scrollLeft())
        });
    };

    // put the tool in place
    placeToolOnTopRightCorner();

    // and listen for scroll:
    // http://stackoverflow.com/questions/8644248/jquery-fix-div-when-browser-scrolls-to-it
    // It's amazing that CSS don't have a fixed-position
    // atributte that's relative to the container ...
    // and for that we have to implement this INPOT:
    canvas.scroll(function(e) {
        placeToolOnTopRightCorner();
    })

    $(window).resize(function(e) {
        //recalculate left
        left = canvas.width() - toolContainer.width() - toolMargin;
        //
        placeToolOnTopRightCorner();
    })

    /**************************************************************************
     * buttons events
     */

    //http://stackoverflow.com/questions/22740228/how-to-scale-jonitjs-graphs
    var btnZoomIn = $("#toolZoomIn");
    btnZoomIn.on("click", function(e) {
        console.log("Zoom In!! There we go!!");
        if(scaleIx < scaleIxMax) {
            scaleIx = scaleIx + 1;
            paper.scale(scales[scaleIx], scales[scaleIx]);
        }

    });

    var btnZoomIn = $("#toolZoom11");
    btnZoomIn.on("click", function(e) {
        console.log("Back to 1:1");
        scaleIx = scaleIx11;
        paper.scale(scales[scaleIx], scales[scaleIx]);
    });

    var btnZoomOut = $("#toolZoomOut");
    btnZoomOut.on("click", function(e) {
        console.log("Zoom Out!! And out we go!!");
        if(scaleIx > scaleIxMin) {
            scaleIx = scaleIx - 1;
            paper.scale(scales[scaleIx], scales[scaleIx]);
        }
    });


});

