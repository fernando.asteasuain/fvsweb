Template.tools.onRendered(function() {

    // we create the toolbar button
    // var container = $('#tools-container');
    // Blaze.render(Template.toolPrecedenceArrow, container.get(0));
    // console.log("Tool Precedence Arrow initialized successfully!!");
    new ButtonRenderLogic(Template.toolPrecedenceArrow, "Tool Precedence Arrow").render()
});

Template.toolPrecedenceArrow.onRendered(function() {

    new ToolLogic(PrecedenceArrowPropertiesEditor, PrecedenceArrowDrawer, {
        id: 'toolPrecedenceArrow',
        name: 'Tool Precedence-Arrow'
    });
});


