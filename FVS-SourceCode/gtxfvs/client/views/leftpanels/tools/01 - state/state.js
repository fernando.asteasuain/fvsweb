
Template.tools.onRendered(function() {

    // we create the toolbar button
    // var container = $('#tools-container');
    // Blaze.render(Template.toolState, container.get(0));
    // console.log("Tool State initialized successfully!!");

    new ButtonRenderLogic(Template.toolState, "Tool State").render()
});

Template.toolState.onRendered(function() {

    new ToolLogic(StatePropertiesEditor, StateDrawer, {
        id: 'toolState',
        name: 'Tool State'
    });
});

