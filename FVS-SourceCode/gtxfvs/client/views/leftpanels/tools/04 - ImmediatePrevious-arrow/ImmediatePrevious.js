Template.tools.onRendered(function() {

    // we create the toolbar button
    // var container = $('#tools-container');
    // Blaze.render(Template.toolImmediatePreviousArrow, container.get(0));
    // console.log("Tool Immediate Previous Arrow initialized successfully!!");
    new ButtonRenderLogic(Template.toolImmediatePreviousArrow, "Tool Immediate Previous Arrow").render()
});

Template.toolImmediatePreviousArrow.onRendered(function() {

    new ToolLogic(ImmediatePreviousArrowPropertiesEditor, ImmediatePreviousArrowDrawer, {
        id: 'toolImmediatePreviousArrow',
        name: 'Tool Immediate Previous Arrow'
    });
});


