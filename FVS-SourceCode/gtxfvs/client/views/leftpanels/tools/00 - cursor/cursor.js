
// Template.tools.onRendered(function() {
//
//     var container = $('#tools-container');
//     Blaze.render(Template.toolCursor, container.get(0));
//     console.log("Tool Cursor initialized successfully!!");
// });

Template.tools.onRendered(function() {
    new ButtonRenderLogic(Template.toolCursor, "Tool Cursor").render()
});

Template.toolCursor.onRendered(function() {

    var select = function(cellView) {

        Selector.selectView(cellView);

        // Problem:
        //    https://api.jquery.com/addclass/
        //    http://stackoverflow.com/questions/8638621/jquery-svg-why-cant-i-addclass
        //    selectedElemView.$el.find('.outer-circle').removeClass('animate-flicker');
        //    selectedElemView.$el.find('.outer-circle').addClass('hide');
        // Solution:
        cellView.$el.find('.outer-circle')
            .attr("class", "outer-circle animate-flicker");

        //
        var selected = graph.getCell(cellView.model.id);
        if(selected) {
            selected._gtxfvs.propertiesEditor.init(selected);
        }
    }

    var unSelect = function() {

        var selectedElemView = Selector.elemView();
        if(selectedElemView) {
            selectedElemView.$el.find('.outer-circle')
                .attr("class", "outer-circle hide");

            var selected = graph.getCell(selectedElemView.model.id);
            if(selected) {
                selected._gtxfvs.propertiesEditor.stop();
            }

        }

        //
        Selector.unselect();
    }

    var onPointerDown = function(){
        //
        unSelect();
    };

    var onPointerUp = function(cellView, evt) {
        //
        unSelect();
        //
        select(cellView);
    };


    // and connect the event
    var button = this.$("#toolCursor");
    button.on("click", function(e) {
        console.log("Tool Cursor (Selection) selected ... and ready to select!!");

        paper.on('cell:pointerup', onPointerUp);
        paper.on('blank:pointerdown', onPointerDown);
    });

    button.bind('deactivated', function(){
        console.log("Tool Cursor stopped");

        paper.off('cell:pointerup', onPointerUp);
        paper.off('blank:pointerdown', onPointerDown);

        unSelect();
    });
});