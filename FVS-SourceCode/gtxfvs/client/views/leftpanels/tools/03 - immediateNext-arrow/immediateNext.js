Template.tools.onRendered(function() {

    // we create the toolbar button
    // var container = $('#tools-container');
    // Blaze.render(Template.toolImmediateNextArrow, container.get(0));
    // console.log("Tool Immediate Next Arrow initialized successfully!!");
    new ButtonRenderLogic(Template.toolImmediateNextArrow, "Tool Immediate Next Arrow").render()
});

Template.toolImmediateNextArrow.onRendered(function() {

    new ToolLogic(ImmediateNextArrowPropertiesEditor, ImmediateNextArrowDrawer, {
        id: 'toolImmediateNextArrow',
        name: 'Tool ImmediateNext-Arrow'
    });
});


