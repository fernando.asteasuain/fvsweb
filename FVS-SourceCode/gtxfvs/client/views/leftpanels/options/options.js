Template.options.onRendered(function() {

    // http://www.curtismlarson.com/blog/2015/04/27/meteor-trigger-function-with-session-variable/
    Tracker.autorun(function() {

        var elemView = Selector.elemView();
        if (elemView) {
            console.log("Element View selected id: " + elemView.id);
            console.log();
        } else {
            console.log("No element selected");
        }
    });


    // OPTIONS - DELETE
    var button = $("#options-delete");
    button.on("click", function(e) {
        console.log("Deleting element ...");

        var selectedElemView = Selector.elemView();
        if(selectedElemView) {
            var selected = graph.getCell(selectedElemView.model.id);
            if(selected) {
                //graph.removeCells([selected]);
                selected._gtxfvs.propertiesEditor.stop();
                selected.remove();
            }
        }
        //
        Selector.unselect();
    });

    // OPTIONS - CLONE

});

Template.options.helpers({

    optionsVisibility: function() {
        var cssClass = "";

        var elemView = Selector.elemView();
        if (!elemView) {
            cssClass = "hidden";
        }

        return cssClass;
    }

});