Package.describe({
  name: 'meteor-jquery-ruler-4fvs',
  version: '0.0.1',
  // Brief, one-line summary of the package.
  summary: '',
  // URL to the Git repository containing the source code for this package.
  git: '',
  // By default, Meteor will default to using README.md for documentation.
  // To avoid submitting documentation, set this field to null.
  documentation: 'README.md'
});

Package.onUse(function(api) {
  api.versionsFrom('1.3.4.4');
  // api.use('ecmascript');
  // api.mainModule('meteor-jquery-ruler-4fvs.js');
  api.use('jquery@1.11.4', 'client');
  api.addFiles(['./source/modernizr-touch.js',
                './source/jquery.ruler.js',
                './source/ruler.css'], 'client');
});

Package.onTest(function(api) {
  // api.use('ecmascript');
  // api.use('tinytest');
  // api.use('meteor-jquery-ruler-4fvs');
  // api.mainModule('meteor-jquery-ruler-4fvs-tests.js');
});
