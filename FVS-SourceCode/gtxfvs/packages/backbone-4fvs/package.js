Package.describe({
  name: 'backbone-4fvs',
  version: '1.2.1',
  summary: '',
  git: '',
  documentation: 'README.md'
});

Package.onUse(function(api) {
  api.versionsFrom('1.3.4.1');

  api.use('lodash-4fvs', 'client');
  api.addFiles('backbone.js', 'client', { transpile: false });
});

Package.onTest(function(api) {

});
