// Import Tinytest from the tinytest Meteor package.
import { Tinytest } from "meteor/tinytest";

// Import and rename a variable exported by meteor-joint-4fvs.js.
import { name as packageName } from "meteor/meteor-joint-4fvs";

// Write your tests here!
// Here is an example.
Tinytest.add('meteor-joint-4fvs - example', function (test) {
  test.equal(packageName, "meteor-joint-4fvs");
});
